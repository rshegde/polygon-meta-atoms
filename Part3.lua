points = #solx/17   --17 because 17 perturbed shapes are given in main. For L36, it should be 37 and so on.
-- print(#solx,#soly,points)
-- print(solx)

spectrum_s = {}
spectrum_p = {}
for j = 0,2,1 do
XY = {}
-- print(j*points+1,j*points+points)
for i = j*points+1,j*points+points,1 do
table.insert(XY,solx[i])
table.insert(XY,soly[i])
end
-- print(j,#XY)







S = S4.NewSimulation()
S:SetLattice({P,0}, {0,P})
S:SetNumG(50)
S:AddMaterial('Air', {1.00059,0})
S:AddMaterial('P_Silicon', {1,0})
S:AddMaterial('C_Silicon',{1,0})
S:AddMaterial('SiO2',{2.1025,0})
S:AddMaterial('A_Silicon', {1,0})
	
S:AddLayer('Air_Top', 0, 'Air')
S:AddLayer('Antenna',t, 'P_Silicon')
S:AddLayer('Substrate', 0.115, 'SiO2')
S:AddLayer('Photo_Diode',0.005,'A_Silicon')


S:SetLayerPatternPolygon('Antenna', 'Air', {0,0}, 0, XY)
	
sum_sqr_err = 0
count = 0
Tra = 0
spm_s = {}
for lam = w_min,w_max,w_step do
	eps_real_PSi,eps_imag_PSi = indices_PSi:Get(lam)
	eps_real_ASi,eps_imag_ASi = indices_ASi:Get(lam)
	eps_real_CSi,eps_imag_CSi = indices_CSi:Get(lam)
	S:SetMaterial('P_Silicon',{eps_real_PSi,eps_imag_PSi})
	S:SetMaterial('A_Silicon',{eps_real_ASi,eps_imag_ASi})
	S:SetMaterial('C_Silicon',{eps_real_CSi,eps_imag_CSi})
	S:SetFrequency(1/lam)
	S:SetExcitationPlanewave({0,0}, {1,0}, {0,0})
	Tra = S:GetPowerFlux('Photo_Diode',0)
	ref_filter = indices_RED:Get(lam)
	diff = Tra-ref_filter*0.8
--	print(lam*1000,Tra)
    table.insert(spm_s,Tra)
	sq_err = diff*diff 
	sum_sqr_err = sum_sqr_err + sq_err
	count = count+1
end
table.insert(spectrum_s,spm_s) --All spectra are appended in the spectrum_s table
if j == 0 then
mean_sqr_err_s = sum_sqr_err/count
-- print(mean_sqr_err_s)
end


end
sigma_s = 0
for i = 2,#spectrum_s,1 do
for j = 1,#spectrum_s[1],1 do
sigma_s = sigma_s+(spectrum_s[i][j] - spectrum_s[1][j])^2
end
end
sigma_s = sigma_s/(#spectrum_s * #spectrum_s[1])
obj_val = mean_sqr_err_s + sigma_s*2
MSE_file:write(tostring(obj_val))
MSE_file:write('\n')
