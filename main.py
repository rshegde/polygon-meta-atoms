# -*- coding: utf-8 -*-
"""
(C) 2020 Soumyashree S. Panda, Ravi S. Hegde

Robust Inverse Design of All-dielectric Metasurface Transmission-mode Color Filters


"""



# This program will run multiple S4 simulations and then extract the results# pylint: disable=C0103
from os import system
import subprocess
import numpy as np
import itertools as itt
import multiprocessing
import time
import math
import de2 as de2
from numpy import *
from scipy.interpolate import interp1d
import random
from matplotlib.patches import Circle, Wedge, Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt
#import mxnet.ndarray as nd
#import mxnet as mx
from itertools import chain

# here we will take the list of all possible parameter values for each parameter
# of interest

#enter these values in nm
P = [350] #This is the lattice Periodicity
t = [80]  #This is the metasurface thickness
Theta = [0] # -- theta orientation in degrees
Phi = [0]  #-- phi orientation in degrees

#These parameters are single valued
w_min = 400  #--Min lambda of the simulation
w_max = 700  # --Max lambda of the simulation
w_step = 10 #--Wavelength steps


points=2 #Number of vertices per octant
D=2*points+1 #Number of variables
N_P=25 #Population per island
N_I=3 #Number of islands
pi=math.pi

#Define the lower and upper limits of variables
L = []
U = []
L_R = zeros(points)+40
L_T = zeros(points)+50
L_P = [0.2]
L.extend(L_R)
L.extend(L_T)
L.extend(L_P)

U_R = zeros(points)+(P[0]/2-40)
U_T = zeros(points)+100
U_P = [pi/4]
U.extend(U_R)
U.extend(U_T)
U.extend(U_P)


def perturb_smooth(txt2):
    pointt = 7
    radi = txt2[4][:len(txt2[4])//2]
    radi1 = radi[::-1]
    radi2 = radi
    radi3 = radi1
    radi4 = radi
    radi5 = radi1
    radi6 = radi
    radi7 = radi1
    radi = np.concatenate((radi,radi1,radi2,radi3,radi4,radi5,radi6,radi7),axis = 0)

    alpha = txt2[4][-1]

    phi = txt2[4][len(txt2[4])//2:-1]
    phi = np.cumsum(phi)
    phi = phi*alpha/phi[-1]
    phi1 = pi/2-phi[::-1]
    phi2 = pi/2+phi
    phi3 = pi-phi[::-1]
    phi4 = pi+phi
    phi5 = 3*pi/2-phi[::-1]
    phi6 = 3*pi/2+phi
    phi7 = -phi[::-1]
    phi = np.concatenate((phi,phi1,phi2,phi3,phi4,phi5,phi6,phi7),axis = 0)
    
    
    L0 = np.array([0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]) #No perturbation
    
    #Define Taguchi's array 
    #Here defined the L16 table suitable for 15 variables with 2 levels (+- 15nm).
    #Last point is kept without perturbation.
    L1 = np.array([-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1, 0])
    L2 = np.array([-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,0])
    L3 = np.array([-1,-1,-1,1,1,1,1,-1,-1,-1,-1,1,1,1,1,0])
    L4 = np.array([-1,-1,-1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,0])
    L5 = np.array([-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,0])
    L6 = np.array([-1,1,1,-1,-1,1,1,1,1,-1,-1,1,1,-1,-1,0])
    L7 = np.array([-1,1,1,1,1,-1,-1,-1,-1,1,1,1,1,-1,-1,0])
    L8 = np.array([-1,1,1,1,1,-1,-1,1,1,-1,-1,-1,-1,1,1,0])
    L9 = np.array([1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,-1,1,0])
    L10 = np.array([1,-1,1,-1,1,-1,1,1,-1,1,-1,1,-1,1,-1,0])
    L11 = np.array([1,-1,1,1,-1,1,-1,-1,1,-1,1,1,-1,1,-1,0])
    L12 = np.array([1,-1,1,1,-1,1,-1,1,-1,1,-1,-1,1,-1,1,0])
    L13 = np.array([1,1,-1,-1,1,1,-1,-1,1,1,-1,-1,1,1,-1,0])
    L14 = np.array([1,1,-1,-1,1,1,-1,1,-1,-1,1,1,-1,-1,1,0])
    L15 = np.array([1,1,-1,1,-1,-1,1,-1,1,1,-1,1,-1,-1,1,0])
    L16 = np.array([1,1,-1,1,-1,-1,1,1,-1,-1,1,-1,1,1,-1,0])
    L = np.array([L0,L1,L2,L3,L4,L5,L6,L6,L8,L9,L10,L11,L12,L13,L14,L15,L16]) #Total 17 models, (1 Original and 16 perturbed)
    
    all_sol_x = []
    all_sol_y = []
    pert = np.zeros((3,size(radi)))
    for i in range(3):
        pert[i] = L[i][:size(radi)]*15+radi   #15nm perturbation is added
        sol_x, sol_y = smoothit(pert[i],phi)  #Edges are smoothed with 2D interpolation
        all_sol_x = np.append(all_sol_x,sol_x)
        all_sol_y = np.append(all_sol_y,sol_y)

    return(all_sol_x,all_sol_y)
    

#Define the 2D interpolation function.
def smoothit (radii,phii):
    xt = radii*np.cos(phii)
    yt = radii*np.sin(phii)
    xt = np.append(xt,xt[0])
    yt = np.append(yt,yt[0])
    x_r = xt
    y_r = yt
    orig_len = len(xt)
    
    xt = np.concatenate((xt[-3:-1], xt , xt[1:3]),axis = 0)
    yt = np.concatenate((yt[-3:-1], yt , yt[1:3]),axis = 0)

    range = np.arange(len(xt))
    ti = np.linspace(2, orig_len + 1, 10 * orig_len)

    xi = interp1d(range, xt, kind='cubic')(ti)
    yi = interp1d(range, yt, kind='cubic')(ti)
    return(xi,yi)





def create_population(L,U,D,N_P,N_I):
    pop = []
    for i in range (N_I):
        bb=[]
        for j in range (N_P):
            sol = np.zeros(D)
            for k in range (D):
                sol[k] =L[k] + np.random.random()*(U[k]-L[k]) 
            bb.append(sol)
        pop.append(bb)   
    return(pop)

population = create_population(L,U,D,N_P,N_I)
isl = list(chain.from_iterable(population))
#print("isl =     ",isl)


# This file gets a param set tuple and a file name
# and writes out the file names in lua

def mfile_writer(idx, pset, mfile):
    sol_x,sol_y = perturb_smooth(pset)
    rfilname = "MSE"+str(idx).zfill(3)+".dat"
#    tfilname = "T"+str(idx).zfill(2)+".dat"
    mfile.write("\n")
    mfile.write("\n")
    mfile.write("MSE_file = io.open(\"" +rfilname+ "\", \"w\") \n")
#    mfile.write("Tfile = io.open(\"" +tfilname+ "\", \"w\") \n")
    mfile.write("\n")
    mfile.write("-- List all the geometrical parameters \n")
    mfile.write("-- Units of all parameters are in micron \n")
    mfile.write("P="+str(pset[0]/1000.0)+"\n")
    mfile.write("t="+str(pset[1]/1000.0)+"\n")
    mfile.write("Theta="+str(pset[2]/1000.0)+"\n")
    mfile.write("Phi="+str(pset[3]/1000.0)+"\n")
    
    
    mfile.write("solx= {")
    for i in range(len(sol_x)):
        if i<len(sol_x)-1:
            mfile.write(str(sol_x[i]/1000.0)+", ")
        else:
            mfile.write(str(sol_x[i]/1000.0))
    mfile.write("}\n")
    
    mfile.write("soly= {")
    for i in range(len(sol_x)):
        if i<len(sol_x)-1:
            mfile.write(str(sol_y[i]/1000.0)+", ")
        else:
            mfile.write(str(sol_y[i]/1000.0))
    mfile.write("}\n")
    
    
	
    mfile.write("\n")
    mfile.write("w_min="+str(w_min/1000.0)+"\n")
    mfile.write("w_max="+str(w_max/1000.0)+"\n")
    mfile.write("w_step="+str(w_step/1000.0)+"\n")

def run_pset(index, comb):
    filestr = "P" + str(index).zfill(3) +".lua"
    midfilestr = "m" + str(index).zfill(3) +".lua"
    mfile = open(midfilestr, 'w')
    #first write the midfile for this set
    mfile_writer(index, comb, mfile)
    mfile.close()

    #make final callable lua file
    filenames = ['Part1.lua', midfilestr, 'Part3.lua']
    # filenames = []
    with open(filestr, 'w') as outfile:
        for fname in filenames:
            with open(fname) as infile:
                outfile.write(infile.read())
    outfile.close()

  # here we will run S4 on the files
    cmd = "S4 " + filestr
    subprocess.call(cmd, shell=True)


t_start = time.time()

def Obj_F(isl,P,t,Theta,Phi):
    par_sets = itt.product(P, t, Theta, Phi, isl)

#    for (x,y) in enumerate(par_sets):
#        run_pset(x,y)
#        print x,y

#    for (index,comb) in enumerate(par_sets):
#        print index, comb

    all_procs = [multiprocessing.Process(target=run_pset, args=(index,comb)) for
             (index, comb) in enumerate(par_sets)]


    for p in all_procs:
        p.start()
    for p in all_procs:
        p.join()

    Output = []
    with open("Results.dat", "w") as Results:
        for i in range(N_P*N_I):
            filename = "MSE"+str(i).zfill(3)+".dat"
#            print(filename)
            with open(filename) as infile:
                temp = infile.readline()
#                print(temp)
                Results.write(temp)
                Output.append( float(temp)  )
    Results.close()

    cmd = "bash cleanup.sh "
    subprocess.call(cmd, shell=True)
    return(Output)

#Obj_F(isl,P,t,Theta,Phi)

#    call file cleanup
#    cmd = "./cleanup.sh "
#    subprocess.call(cmd, shell=True)

args = (P,t,Theta,Phi)



de2.de_isl(Obj_F, P, t, Theta, Phi,L,U, num_layers_i = D, num_isles = N_I, num_gens = 5,
	  poplist = population, mut = 0.8, crossp = 0.7, popsize = N_P,
	  its = 20 , lenp = 0.08, lins = 0.06, verbose = 0)

t_end = time.time()
print("Simulation ran in " + str((t_end - t_start)/3600.0)+"hours\n")


