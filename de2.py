import numpy as np
import random
#import mxnet.ndarray as nd
#import mxnet as mx

from itertools import chain


def lengthen_best(best):
    return np.append(best, np.random.rand())

def ins_best(best):
    ins_loc = np.random.choice(best.size)
    return np.insert(best, (ins_loc, ins_loc), np.random.uniform(0,1,2))

def pad_chrom(inp, max_len):
    if inp.size < max_len:
        #ins_loc = np.random.choice(inp.size, max_len - inp.size, replace=False)
        
        ins_val = np.zeros(max_len - inp.size)
        #ins_val = np.random.uniform(0,1, max_len - inp.size)
        return np.append(inp, ins_val)
    else:
        return inp


def cre_mutant(best, b, c, mut, L, U):
#     max_len = max( best.size, b.size, c.size)
#     best_n = pad_chrom(best, max_len=max_len)
#     b_n = pad_chrom(b, max_len=max_len)
#     c_n = pad_chrom(c, max_len=max_len)
    #return np.clip(best_n + mut*(b_n - c_n), 0, 1)
    mut_V = np.zeros(len(best))
    for i in range(len(best)):
        mut_V[i] = np.clip(best[i] + mut*(b[i] - c[i]),L[i],U[i] )
    return mut_V


def crossov(mutant, curr, crossp):
#     max_len = max( curr.size, mutant.size)
#     if curr.size == mutant.size:
#         c_r = curr
#         m_r = mutant
#     else:
#         c_r = pad_chrom(curr, max_len)
#         m_r = pad_chrom(mutant, max_len)
    
    cross_points = np.random.rand(curr.size) > crossp
#     if not np.any(cross_points):
#         cross_points[np.random.randint(0, curr.size)] = True
    #return np.where(cross_points, m_r, c_r)
    return np.where(cross_points, mutant, curr)


def de_isl(
    fobj,  # objective function, serial eval
    P,t,Theta,Phi,L,U,
    num_layers_i=16,
    num_isles = 5, #number of islands
    num_gens =5,
    poplist=[],  # already initialized population
    mut=0.8,  # mutation rate
    crossp=0.7,  # crossover
    popsize=20,  # the population size
    its=1000, # the number of iterations needed to run for
    lenp=0.08,
    lins=0.06,
    verbose=0
):

    """
        This function performs diff evolution using fobj evaluated in parallel  
        returns the  last pop, fitness of pop, best individual and opt history 
    """
    #mut = 0.8 + 0.6*np.random.random()
    history = []
    bids = np.zeros(num_isles, dtype=int)
    bfits = np.zeros(num_isles)
    mut_r = np.random.uniform(0.4, 0.4, num_isles)
    cro_r = np.random.uniform(0.5, 0.5, num_isles)
    num_func_evals = 0
  
    trilist = []
    for isl in range(num_isles):
        cc=[]
        for j in range(popsize):
            sol1=np.zeros(num_layers_i)
            for k in range(num_layers_i):
                sol1[k] = L[k]+np.random.random()*(U[k]-L[k])
            cc.append(sol1)
        trilist.append(cc)
#    print("trilist=    ",trilist)
#         print(tmp)
#        trilist.append( np.split(tmp, popsize) )
        
        
    tmp2 = np.random.uniform(0,1, num_isles*num_layers_i)
    bests = np.split(tmp2, num_isles)
#    print("bests=   ",bests)
    

    for gen in range(num_gens):
        if verbose == 0:
            print("==============================")
            print("Epoch #:" + str(gen + 1))
            print("==============================")
        with open("Data_best.txt","a") as data:
            data.write("Generation : "+ str(gen))
        data.close()
        with open("Data_bfits.txt","a") as data1:
            data1.write("Generation : "+str(gen))
        data1.close()        
        isl = list(chain.from_iterable(poplist))
#         print("\n isl=",isl)
        
        fitness_isl = np.asarray(fobj(isl,P,t,Theta,Phi))
        num_func_evals+=len(isl)
#         fitness_isl=np.transpose(fitness_isl_1)
        fitness = np.split(fitness_isl, num_isles)
#         print(fitness)
        
        for isln in range(num_isles):
            bids[isln] = np.argmin(fitness[isln])
            bests[isln] = poplist[isln][bids[isln]]
#         print("\n bids bests",bids,bests)  
        
        for i in range(its):
            for isln in range(num_isles):
                for j in range(popsize):
                    idxs = [idx for idx in range(popsize) if idx != j]
                    picks = np.random.choice(idxs, 3, replace = False)
#                     print(j,picks)
                    a, b, c = poplist[isln][picks[0]],  poplist[isln][picks[1]], poplist[isln][picks[2]]
                    mutant = cre_mutant(a, b, c, mut_r[isln],L,U)
                    child = crossov(mutant, poplist[isln][j], cro_r[isln])
#                    print("Mutant ",j,mutant)
#                    print("Child ",j,child)
#                      if np.random.rand() < 0.5:
#                         mutant = cre_mutant(a, b, c, mut_r[isln])
#                         child = crossov(mutant, poplist[isln][j], cro_r[isln])

#                     elif np.random.rand() < lenp:
#                         best_n = lengthen_best(bests[isln])
#                         print("\n ",best_n,"\n",b,c,mut_r[isln])
#                         mutant = cre_mutant(best_n, b, c, mut_r[isln])
#                         child = crossov(mutant, poplist[isln][j], cro_r[isln])
#                     else:
#                         mutant = cre_mutant(bests[isln], b, c, mut_r[isln])
#                         child = crossov(mutant, poplist[isln][j], cro_r[isln])
#                         if np.random.rand() <=lins:
#                             child = ins_best(child)

                    trilist[isln][j] = child


#            print("Gadbad=   ",trilist)
            tflat = list(chain.from_iterable(trilist))
            f_isl = np.asarray(fobj(tflat,P,t,Theta,Phi))
#             f_isl = np.transpose(f_isl_1)
#             print("\n asdfg",f_isl)

            # print("F_isl =  ",f_isl) 

            f_isl_gpu = f_isl

            f = np.split(f_isl, num_isles)
            num_func_evals+=len(tflat)
            
#             print("***********TRILIST***************") 
#             print(trilist)
#             print("***********POPLIST***************")
#             print(poplist)
#            print("Trilist Fitness = " ,f,"\n","Population Fitness =",fitness)
            for isln in range(num_isles):
                for j in range(popsize):
#                     print(i,j,f[isln][j],fitness[isln][j])
                    if (f[isln][j] < fitness[isln][j]):
                        fitness[isln][j] = f[isln][j]
                        poplist[isln][j] = trilist[isln][j]
                        if f[isln][j] < fitness[isln][bids[isln]]:
                            bids[isln] = j
                            bests[isln] = trilist[isln][j]

                bfits[isln] = fitness[isln][bids[isln]]

#             print("**********UPDATED POPLIST***********\n",poplist)
            if (i+1)%1 == 0:
                print("Iteration = ","%3d" %i,  " -- ")
                for num in bfits:
                    print("Best fit = ","%4.6f" %num, "  ")
                print()
            with open("Data_bfits.txt","a") as data:
                data.write(str(i)+"\t"+str(bfits)+"\n")
            data.close()
            
            with open("Data_best.txt","a") as data1:
                data1.write(str(i)+"\t"+str(bests)+"\n")
            data1.close()
            history.append(np.copy(bfits))



        if its > 64:
            its = int(its/2)



        if gen < (num_gens - 1):
            #print("Round robin best migration")
            stmp = np.copy(poplist[num_isles-1][bids[num_isles-1]])
            for isln in range(num_isles-1, 0, -1):

                poplist[isln][bids[isln]] = np.copy(poplist[isln-1][bids[isln-1]])
            poplist[0][bids[0]] = stmp 


    print("Num func evals: ", num_func_evals)
    print("bid = ",bids,"bests = ", bests,"bfits = ", bfits,"history = ", np.asarray(history))
    return bids, bests, bfits, np.asarray(history), num_func_evals





